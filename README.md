# Node Todo App

A Node app built with MongoDB and Angular.

Node provides the RESTful API. Angular provides the frontend and accesses the API. MongoDB stores like a hoarder.

Just use Stylus preprocessor for CSS.

Powered by [ljossha (Alexey Sechko)](https://vk.com/notedamus)

## Requirements

- [Node and npm](http://nodejs.org)
- MongoDB: Make sure you have your own local or remote MongoDB database URI configured in `config/database.js`

## Installation

1. Clone the repository: `git clone https://github.com/ljossha/Node-Todo`
2. Install the application: `npm install`
3. Place your own MongoDB URI in `config/database.js`
3. Start the server: `npm start`
4. View in browser at `http://localhost:8080`

![Node-Todo](todo.png)



